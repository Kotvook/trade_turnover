﻿using System;
using System.Collections.Generic;
using System.Text;

namespace trade_turnover.Entities
{
    enum DiscountType
    {
        NoDiscount = 0,
        Common = 1,
        Transport = 2,
        Bonus = 3
    }
}
