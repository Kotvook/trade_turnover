﻿using System;
using System.Collections.Generic;
using System.Text;
using trade_turnover.Interfaces;

namespace trade_turnover.Entities
{
    class ProductTwo : IProduct
    {
        public string Name { get; }
        public decimal Price { get; }

        public ProductTwo()
        {
            this.Name = "Poduct Two";
            this.Price = 31;
        }
    }
}
