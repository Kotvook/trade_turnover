﻿using System;
using System.Collections.Generic;
using System.Text;
using trade_turnover.Interfaces;

namespace trade_turnover.Entities
{
    class Order
    {
        /// <summary>
        /// Determines the order of a certain product.
        /// <value> Product - product type</value>
        /// <value> Quantity - product quantity</value>
        /// <value> Discount - size of the discount and its type</value>
        /// <value> Date - order date</value>
        /// </summary>
        public IProduct Product { get; }
        public int Quantity { get; }
        public Discount Discount { get; }
        public DateTime Date { get;}

        public Order(IProduct product, int quantity, Discount discount, DateTime orderDateTime)
        {
            this.Product = product;
            this.Quantity = quantity;
            this.Discount = discount;
            this.Date = orderDateTime;
        }

        public Order(IProduct product, int count, DateTime orderDateTime)
        {
            this.Product = product;
            this.Quantity = count;
            this.Date = orderDateTime;
            this.Discount = new Discount(DiscountType.NoDiscount);
        }

        /// <summary>
        /// Calculates the cost of the product with a discount, depending on the type of discount.
        /// </summary>
        /// <returns>The cost of the product with a discount</returns>
        public decimal GetDiscount()
        {
            return this.Discount.DiscountType switch
            {
                DiscountType.Common => Product.Price * Quantity * (1 - Discount.Value / 100),
                DiscountType.Transport => Product.Price * Quantity - Discount.Value,
                DiscountType.Bonus => Product.Price * (Quantity - 1),
                _ => Product.Price * Quantity,
            };
        }

        /// <summary>
        /// Calculates the full cost of the order without discounts.
        /// </summary>
        /// <returns>сost of the order without discounts</returns>
        public decimal GetFullPrice()
        {
            return Product.Price * Quantity;
        }
    }
}
