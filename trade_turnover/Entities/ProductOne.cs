﻿using System;
using System.Collections.Generic;
using System.Text;
using trade_turnover.Interfaces;

namespace trade_turnover.Entities
{
    class ProductOne: IProduct
    {
        public string Name { get; }
        public decimal Price { get; }

        public ProductOne()
        {
            this.Name = "Poduct One";
            this.Price = 15;
        }

    }
}
