﻿using System;
using System.Collections.Generic;
using System.Text;

namespace trade_turnover.Entities
{
    class Discount
    {
        public decimal Value { get; }
        public DiscountType DiscountType { get; set; }

        public Discount(decimal value, DiscountType discountType)
        {
            this.Value = value;
            this.DiscountType = discountType;
        }

        public Discount(DiscountType discountType)
        {
            if (discountType != DiscountType.Bonus && discountType != DiscountType.NoDiscount)
            {
                throw new ArgumentException("Just DiscountType.Bonus can be without value of discount");
            }
            this.DiscountType = discountType;
        }
    }
}
