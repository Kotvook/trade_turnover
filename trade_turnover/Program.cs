﻿using System;
using trade_turnover.Entities;

namespace trade_turnover
{
    class Program
    {
        static void Main(string[] args)
        {
            PurchaseProducts purchaseProducts = new PurchaseProducts(new ProductOne(), Month.August);
            purchaseProducts.PrintAll();
            purchaseProducts.PrintOnDay(10);
            purchaseProducts.PrintSortedByCatecory();
            purchaseProducts.PrintCategory(DiscountType.Transport);
        }
    }
}
