﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using trade_turnover.Entities;
using trade_turnover.Interfaces;

namespace trade_turnover
{
    class PurchaseProducts
    {
        /// <summary>
        /// Describes purchases of a specific product in a given month
        /// The data is sorted by date in the class constructor <see cref="PurchaseProducts">Construcor</see>
        /// <value> Product - contains an object of the class that implements the interface IProduct</value>
        /// <value> Month - contains one element of the Month enumeration</value>
        /// <value> Orders - сontains a list of all purchases of a certain product in a given month</value>
        /// </summary>
        private readonly IProduct Product;
        private readonly Month Month;
        private readonly List<Order> Orders = new List<Order>();

        public PurchaseProducts(IProduct product, Month month)
        {
            this.Product = product;
            this.Month = month;
            this.Orders.AddRange(from ord in AllData()
                                 where (ord.Product.GetType() == product.GetType())
                                 where ord.Date.Month == (int)Month
                                 orderby ord.Date
                                 select ord);
        }

        /// <summary>
        /// Creates a shopping list.
        /// </summary>
        /// <returns>Full shopping list</returns>
        private List<Order> AllData()
        {
            List<Order> allData = new List<Order>
            {
                new Order(new ProductOne(), 5, new Discount(14, DiscountType.Common), new DateTime(2021, 8, 1)),
                new Order(new ProductOne(), 10, new DateTime(2021, 8, 12)),
                new Order(new ProductOne(), 3, new DateTime(2021, 8, 10)),
                new Order(new ProductOne(), 12, new DateTime(2021, 7, 1)),
                new Order(new ProductOne(), 5, new DateTime(2021, 8, 10)),
                new Order(new ProductOne(), 5, new DateTime(2021, 5, 21)),
                new Order(new ProductOne(), 5, new Discount(14, DiscountType.Common), new DateTime(2021, 8, 1)),
                new Order(new ProductOne(), 10, new Discount(10, DiscountType.Transport), new DateTime(2021, 8, 14)),
                new Order(new ProductOne(), 5, new Discount(5, DiscountType.Transport), new DateTime(2021, 8, 14)),
                new Order(new ProductOne(), 10, new Discount(DiscountType.Bonus), new DateTime(2021, 8, 5)),
                new Order(new ProductOne(), 23, new Discount(DiscountType.Bonus), new DateTime(2021, 8, 5)),
                new Order(new ProductOne(), 22, new Discount(15, DiscountType.Common), new DateTime(2021, 8, 5)),
                new Order(new ProductTwo(), 10, new Discount(DiscountType.Bonus), new DateTime(2021, 8, 5)),
                new Order(new ProductTwo(), 23, new Discount(10, DiscountType.Common), new DateTime(2021, 8, 5)),
                new Order(new ProductOne(), 21, new DateTime(2021, 4, 15)),
                new Order(new ProductOne(), 21, new DateTime(2021, 4, 15))
            };

            return allData;
        }

        /// <summary>
        /// Displays all data of purchases of a certain product in a given month to the console.
        /// </summary>
        public void PrintAll()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\nAll purchases:");
            Console.ResetColor();
            Print(this.Orders);
        }

        /// <summary>
        /// Displays all data of purchases but sorted by discount category.
        /// </summary>
        public void PrintSortedByCatecory()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\nPurchases sorted by category:");
            Console.ResetColor();

            List<Order> data = new List<Order>(from d in this.Orders orderby d.Discount.DiscountType select d);
            Print(data);
        }

        /// <summary>
        /// Displays data on purchases with a certain category of discounts.
        /// </summary>
        /// <param name="category">Сategory of discounts</param>
        public void PrintCategory(DiscountType category)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"\nPurchases with discount {category}:");
            Console.ResetColor();

            List<Order> data = new List<Order>(from d in this.Orders
                                               where d.Discount.DiscountType == category
                                               select d);

            Print(data);
        }

        /// <summary>
        /// Displays all the data about purchases on a certain day of the month.
        /// </summary>
        /// <param name="day">Day of purchases</param>
        public void PrintOnDay(byte day)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"\nPurchases on {Month}, {day}:");
            Console.ResetColor();

            List<Order> data = new List<Order>(from d in this.Orders
                                                 where d.Date.Day == day
                                                 select d);
            Print(data);
        }

        

        /// <summary>
        /// An internal method that displays the specified shopping list to the console.
        /// </summary>
        /// <param name="data">The list of purchases to display on the console</param>
        private void Print(List<Order> data)
        {
            if (data.Count == 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"\nNo Data!\n");
                Console.ResetColor();
                return;
            }

            Console.WriteLine($"Name: {Product.Name}, Price: {Product.Price} BYN\n");
            Console.WriteLine($"Day of month({Month})\t" +
                $"Quantity\t" +
                $"Price\t\t" +
                $"Price With Diccount\n" +
                $"{new string('-', 75)}\n");

            foreach (Order order in data)
            {
                Console.Write($"\t{order.Date.Day}\t\t   {order.Quantity}\t\t{order.GetFullPrice()} BYN\t\t{order.GetDiscount()} BYN ({order.Discount.DiscountType})\n");
            }
        }

        /// <summary>
        /// Adds a new batch of purchases.
        /// </summary>
        /// <param name="number">The number of products in the order</param>
        /// <param name="discount">The type and value of the discount</param>
        /// <param name="dateTime">Date of purchase</param>
        public void AddNewOrder(int number, Discount discount, DateTime dateTime)
        {
            this.Orders.Add(new Order(this.Product, number, discount, dateTime));
        }
    }
}
