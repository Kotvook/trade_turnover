﻿using System;
using System.Collections.Generic;
using System.Text;

namespace trade_turnover.Interfaces
{
    interface IProduct
    {
        string Name { get; }
        decimal Price { get; }
    }
}
